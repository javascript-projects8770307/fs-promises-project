const fs = require('fs');

function createDirectory() {
    return new Promise((resolve, reject) => {
        fs.mkdir('jsonDirectory', (err) => {
            if (err) {
                reject("Unable to create Directory or it is already available!!");
            } else {
                resolve("Directory created Successfully");
            }
        })
    })
}

function createNewFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.writeFile('./jsonDirectory/' + fileName, '', (err) => {
            if (err) {
                console.log(err);
                reject("Unable to Create Files");
            } else {
                console.log(`${fileName}.json created successfully`);
                resolve(`Creating Json files...`);
            }
        })
    })
}

function createFiles() {
    return new Promise((resolve, reject) => {
        let numberOfFiles = parseInt(Math.random() * 20);
        const filesArray = Array.from(Array(numberOfFiles).keys());
        if (filesArray.length < 1) {
            reject("Files less than 1");
        } else {
            let files = filesArray.map(file => {
                resolve(createNewFile(file + '.json'));
                return file;
            })
            console.log(`Created ${files.length} json files`);
        }

    })
}

function deleteFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.unlink('jsonDirectory/' + fileName, (err) => {
            if (err) {
                console.log(err);
                reject('Unable to delete Files');
            } else {
                console.log(`${fileName}.json deleted successfully`);
                resolve(`Deleting Files...`);
            }
        })
    })
}

function deleteFiles() {
    return new Promise((resolve, reject) => {
        fs.readdir('jsonDirectory', (err, filesData) => {
            if (err) {
                reject('No Directory Available');
            } else {
                let files = filesData.map(file => {
                    resolve(deleteFile(file));
                    return file;
                })
                console.log(`Total ${files.length} files to be deleted`);
            }
        })
    })
}

function checkPromise() {
    createDirectory().then(message => {
        console.log(message);
        return createFiles();
    }).then(message => {
        console.log(message);
        return deleteFiles();
    }).then(message => {
        console.log(message);
    }).catch(message => {
        console.log(message);
    })
}
module.exports = checkPromise;