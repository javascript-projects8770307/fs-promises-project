const fs = require('fs');

function readFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (err, lipsumContent) => {
            if (err) {
                console.log(err);
                reject("Unable to read File");
            } else {
                resolve(lipsumContent);
            }
        })
    })
}

function createUpperCase(content) {
    return new Promise((resolve, reject) => {
        if (!content) {
            reject("Unable to change to upper Case");
        } else {
            let upperCaseContent = content.toUpperCase();
            resolve(upperCaseContent);
        }
    })
}

function createLowerCase(content) {
    return new Promise((resolve, reject) => {
        if (!content) {
            reject("Unable to change to Lower case");
        } else {
            let lowerCaseContent = content.toLowerCase();
            console.log(lowerCaseContent);
            resolve(lowerCaseContent);
        }
    })
}

function writeinFile(fileName, fileContent) {
    return new Promise((resolve, reject) => {
        fs.writeFile(fileName, fileContent, (err) => {
            if (err) {
                console.log(err);
                reject("Unable to write into the file");
            } else {
                console.log("File Created Successfully");
                resolve(fileName);
            }
        })
    })
}

function writeFileNames(fileName) {
    return new Promise((resolve, reject) => {
        fs.appendFile('filenames.txt', fileName + '\n', (err) => {
            if (err) {
                console.log(err);
                reject('Unable write in file');
            } else {
                resolve("Successfully Added " + fileName + " in filenames.txt");
            }
        })
    })
}

function splitIntoSentences(content) {
    return new Promise((resolve, reject) => {
        if (!content) {
            reject("Unable to split into sentences");
        } else {
            let sentences = content.split('.');
            let newContent = JSON.stringify(sentences);
            resolve(newContent);
        }
    })
}

function sortContent(content) {
    return new Promise((resolve, reject) => {
        if (!content) {
            reject("Unable to sort the content");
        } else {
            let contentArray = JSON.parse(content);
            let sortedContent = contentArray.sort((first, second) => {
                if (first < second) {
                    return -1;
                } else if (first > second) {
                    return 1;
                } else {
                    return 0;
                }
            })
            resolve(JSON.stringify(sortedContent));
        }
    })
}

function deleteFile(file){
    return new Promise((resolve,reject)=>{
        fs.unlink(file, (err) => {
            if (err) {
                reject("Unable to delete files");
            } else {
                console.log(`${file} deleted Successfully`);
                resolve("Deleting Files...");
            }
        })
    })
}


function deleteFiles(files) {
    return new Promise((resolve, reject) => {
        if (!files) {
            reject("Can't delete! no files provided");
        } else {
            files.map(file => {
                return resolve(deleteFile(file));
            });
        }
    })
}


function checkPromise() {

    readFile('./lipsum.txt').then(content => {
        console.log(content);
        return createUpperCase(content);
    })
        .then(content => {
            console.log(content);
            return writeinFile('UpperCase.txt', content);
        })
        .then(fileName => {
            console.log(fileName);
            return writeFileNames(fileName);
        })
        .then(message => {
            console.log(message);
            return readFile('UpperCase.txt');
        })
        .then(content => {
            console.log(content);
            return createLowerCase(content);
        })
        .then(content => {
            console.log(content);
            return splitIntoSentences(content)
        })
        .then(sentences => {
            console.log(sentences);
            return writeinFile('Sentences.txt', sentences);
        })
        .then(fileName => {
            console.log(fileName);
            return writeFileNames(fileName);
        })
        .then(message => {
            console.log(message);
            return readFile('Sentences.txt');
        })
        .then(content => {
            console.log(content);
            return sortContent(content);
        })
        .then(content => {
            console.log(content);
            return writeinFile('SortedContent.txt', content);
        })
        .then(fileName => {
            console.log(fileName);
            return writeFileNames(fileName);
        })
        .then(message => {
            console.log(message);
            return readFile('filenames.txt');
        })
        .then(allFiles => {
            console.log(allFiles);
            filesArray = allFiles.split('\n');
            filesArray.pop();
            return deleteFiles(filesArray);

        })
        .then(message => {
            console.log(message);
        })
        .catch(message => {
            console.log(message);
        })
}

module.exports = checkPromise;